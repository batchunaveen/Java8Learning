package com.core.lambda;

/**
 * Created by naveen on 11/16/17.
 */

@FunctionalInterface
interface CalculationInterface<A>{
    public abstract A sumMethod(A val1, A val2);
}


public class SumOfNumbers {
    public static void main(String args[]){

        CalculationInterface<Integer> sum = (Integer val1, Integer val2) -> {
            return val1+val2;
        };

        //Call sumMethod
        Integer result = sum.sumMethod(2,3);
        System.out.println(result);

    }

}
