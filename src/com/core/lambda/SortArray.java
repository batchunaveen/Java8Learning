package com.core.lambda;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by naveen on 11/15/17.
 */
public class SortArray {
    public static void main(String args[]){

        String[] stringArrary = {"ab", "ef","cd"};

   /*     //Create Local CLass
        class StringSort implements Comparator<String> {
           public int compare(String a, String b){
               return a.compareTo(b);
           }
        }*/

     /*   System.out.println("Before Java 8 " +
                "Sorting Array using " +
                "Local Class without lambda expression");
*/
      //  Arrays.sort(stringArrary,new StringSort());


        System.out.println("In java 8 lambda expression");
     /*   Arrays.sort(stringArrary, (String a, String b) -> {
            return a.compareTo(b);
        });*/
        Arrays.sort(stringArrary,(a,b)->a.compareTo(b));

        //Display String Array
     /*   for(String str : stringArrary){
            System.out.println(str+"");
        }
*/

        List<String> stringList = Arrays.asList(stringArrary);

        Collections.sort(stringList,(a,b)->a.compareTo(b));

        System.out.println(stringList);





    }

}
