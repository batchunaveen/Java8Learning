/**
 * Created by naveen on 11/13/17.
 */
package com.core.java8;

@FunctionalInterface
public interface IBankAccount {
    public abstract void amount();
}
