package com.core.java8;

public class Main {

    public static void main(String[] args) {

        IBankAccount bankAccount = new IBankAccount() {
            @Override
            public void amount() {
                System.out.print("My Amount");
            }
        };

        IBankAccount bankAccount1 =  () -> {
            System.out.println("Java 8 way of doing");
        };


        bankAccount.amount();
        bankAccount1.amount();
    }
}
