
/**
 * Created by naveen on 11/15/17.
 */
package com.core.java.innerclasses;

public class OuterClass {


    int i = 1; //instance variable
    void methodA(){}//Instance method

    static int value = 1; //Static variable
    static void staticMethod() {}// Static method

    //Inner Class
    class InnerClass {

        public void method() {

            System.out.println("In Inner class Method");

            i =1; //Outer class Instance variable
            methodA(); // oc instance method

            value = 1;// OC static variable
            staticMethod();// OC static method

            System.out.println("OuterClass reference="+OuterClass.this);
            System.out.println("InnerClass reference="+this);

            OuterClass.this.i=2;//Accessing OuterClass instanceVariable using OuerClass reference
            OuterClass.this.methodA();//Accessing OuterClass instanceMethod using OuterClass reference
        }

    }

}



