package com.core.java.innerclasses;

/**
 * Created by naveen on 11/15/17.
 */
public class NestedClass {


    static int staticI = 1;
    static void staticMethod(){}

    // StaticNestedClass
    static class StaticNestedClass {



        // StaticNestedClass can declare static initialization blocks
        static {
        }

        // StaticNestedClass can declare member interfaces.
        interface I {
        }

        // StaticNestedClass can declare static members
        static int i = 2;

        // StaticNestedClass can declare constant variables
        static final int j = 3;

        //StaticNestedClass classes can declare instance initialization blocks
        {}

        //StaticNestedClass constructor
        StaticNestedClass() {}


        public void method(){
            System.out.println("In static nested Class");
            staticI = 1;//OuterClass static variable
            staticMethod(); //OuterClass static method

            System.out.println("Vaues"+staticI);
        }





    }
}
